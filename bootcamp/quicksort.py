def quicksort(array):
    if len(array) < 2 :
        return array
    pivot = array[len(array)/2]
    array.remove(pivot)
    less = []
    greater = []
    for x in array:
        if x <= pivot :
            less.append(x)
        else:
            greater.append(x)
    return quicksort(less) + [pivot] + quicksort(greater)
