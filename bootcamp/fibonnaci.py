def fibonacci(n=10):
    print('First %s terms of the Fibonacci sequence:' % n)
    fib = [1, 1]
    i = 2
    while i < n :
        fib.append(fib[i-2] + fib[i-1])
        i = i + 1
    print fib[:n]
